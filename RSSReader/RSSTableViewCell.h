//
//  RSSTableCellTableViewCell.h
//  RSSReader
//
//  Created by Андрей on 12.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSSTableViewCell : UITableViewCell {}

@property (nonatomic, strong) IBOutlet UIImageView *rssImage;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
