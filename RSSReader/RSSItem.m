//
//  RSSItem.m
//  RSSReader
//
//  Created by Андрей on 18.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSItem.h"

@implementation RSSItem
-(id) init{
    self = [super init];
        _link = [[NSMutableString alloc] init];
        _title = [[NSMutableString alloc] init];
        _pubDate = [[NSMutableString alloc] init];
        _description = [[NSMutableString alloc] init];
        _firstImageUrl = [[NSMutableString alloc] init];
    return self;
}
@end
