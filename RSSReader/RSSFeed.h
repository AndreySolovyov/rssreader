//
//  RSSFeed.h
//  RSSReader
//
//  Created by Андрей on 18.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

//Представляет список новостей
@interface RSSFeed : NSObject

@property NSString *channelUrl;

@property NSMutableString *channelTitle;
@property NSMutableString *channelImageUrl;

@property NSMutableArray *items;
@property NSCache *imagesCache;

@end
