//
//  RSSAppDelegate.h
//  RSSReader
//
//  Created by Андрей on 11.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
