//
//  RSSFeedUrlValidator.h
//  RSSReader
//
//  Created by Андрей on 26.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSFeed.h"

@interface RSSFeedAllowanceChecker : NSObject

-(BOOL)isFeedUrlValid:(NSString*)url;

-(NSString*)isFeedUrlAlreadyAdded:(NSString*)url toFeedsArray:(NSArray*)availableFeeds;

-(BOOL)isFeedPotentiallyNotAFeed:(RSSFeed*)feed;

@end
