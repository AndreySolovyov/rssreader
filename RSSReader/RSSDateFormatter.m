//
//  RSSDateFormatter.m
//  RSSReader
//
//  Created by Андрей on 16.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSDateFormatter.h"



@implementation RSSDateFormatter

static NSLocale *usLocale;
static NSString *const rssDateFormat = @"EEE, dd MMM yyyy HH:mm:ss zzz";
static NSString *const formatString = @"dd-MM-yyyy";

+(void)initialize{
    if(self == [RSSDateFormatter class]){
        usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    }
}

//Строка - Дата - Строка
+(NSString*) formatDateFromString:(NSString*)dateAsString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale: usLocale];
    [dateFormatter setDateFormat:rssDateFormat];
    
    NSDate *temporaryDate = [dateFormatter dateFromString:dateAsString];
    
    [dateFormatter setDateFormat:formatString];
    NSString *formattedDateString = [dateFormatter stringFromDate:temporaryDate];
    
    //На всякий случай, вдруг дата не отформатируется и вернется nil - просто отдаем исходную дату
    return formattedDateString!=nil?formattedDateString:dateAsString;
    
}

@end
