//
//  RSSDetailViewController.m
//  RSSReader
//
//  Created by Андрей on 11.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSItemTableViewController.h"

@interface RSSItemTableViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@end

@implementation RSSItemTableViewController

#pragma mark - Managing the detail item

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSURL *myURL = [NSURL URLWithString: self.url];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:myURL];
    self.webView.delegate  = self;
    [self.webView loadRequest:request];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
