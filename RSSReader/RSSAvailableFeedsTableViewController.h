//
//  RSSAvailableFeedsTableViewController.h
//  RSSReader
//
//  Created by Андрей on 24.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSSAvailableFeedsTableViewController : UITableViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *addNewFeedTextField;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *feedPendingInProgressActivityIndicator;

@end
