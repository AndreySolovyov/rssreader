//
//  RSSFeedParser.m
//  RSSReader
//
//  Created by Андрей on 15.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSItem.h"
#import "RSSFeedParser.h"
#import "RSSDateFormatter.h"

@interface RSSFeedParser () {
    NSXMLParser *parser;
    NSString *element;
    
    RSSFeed *feed;
    RSSItem *item;
}
@end


@implementation RSSFeedParser

-(id)init{
    self = [super init];
    parser = [NSXMLParser alloc];
    return self;
}

// Обрабатываем начало элемента
- (void) parser:(NSXMLParser*) parser didStartElement:(NSString*) elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    element = elementName;
    
    if([element isEqualToString:@"channel"]){
        if(feed == nil){
            feed = [[RSSFeed alloc] init];
        }
    }
    else if([element isEqualToString:@"item"])
    {
        if(feed != nil){
            item = [[RSSItem alloc] init];
        }
    }
    else if([element isEqualToString:@"enclosure"])
    {
        if(item != nil){
            if([item.firstImageUrl length] == 0){
                NSString *enclosureType = [attributeDict valueForKey:@"type"];
                if([enclosureType hasPrefix:@"image/"]){
                    [item setFirstImageUrl:[attributeDict valueForKey:@"url"]];
                }
            }
        }
    }
}

//Обрабатываем текст, содержащийся внутри элемента
-(void)parser: (NSXMLParser *)parser foundCharacters:(NSString *)string{
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(item != nil){
        if([element isEqualToString:@"title"]){
            [item.title appendString:string];
        }
        else if([element isEqualToString:@"link"]){
            [item.link appendString:string];
        }
        else if([element isEqualToString:@"pubDate"]){
            [item.pubDate appendString:string];
        }
        else if([element isEqualToString:@"description"]){
            [item.description appendString:string];
        }
    } else if(feed != nil){
        if([element isEqualToString:@"title"]){
            if(feed.channelTitle == nil){
                feed.channelTitle = [[NSMutableString alloc] init];
                [feed.channelTitle appendString:string];
            }
        } else if([element isEqualToString:@"url"]){
            if(feed.channelImageUrl == nil){
                feed.channelImageUrl = [[NSMutableString alloc] init];
            }
            [feed.channelImageUrl appendString:string];
        }
    }
}

- (void) parser:(NSXMLParser*) parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if([elementName isEqualToString:@"item"]){
        if(![self newItemAlreadyExists:item]){
            [feed.items addObject:item];
        }
        //сбрасываем item. это нужно для того, чтобы, например, найти навзвание фида, а не отдельной новости
        //если мы набредаем на title, но item при этом пустой, считаем это названием фида.
        item = nil;
    }
}

//Два следующих метода инициируют парсер содержимым URL, либо файла
//и запускают парсинг
-(RSSFeed*) parseFeedFromContentsOfFile:(NSString*) path withType:(NSString*) type{
    NSString *rssFilePath = [[NSBundle mainBundle] pathForResource:path ofType:type];
    NSData *rssFileData = [NSData dataWithContentsOfFile:rssFilePath];
    
    parser = [parser initWithData:rssFileData];
    [self doParse];
    
    return feed;
}

-(RSSFeed*) parseFeedFromUrl:(NSString*) urlString{
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    parser = [parser initWithContentsOfURL:url];
    [self doParse];
    
    return feed;
}

-(RSSFeed*) populateFeed:(RSSFeed*) givenFeed{
    if(givenFeed == nil){
        NSLog(@"Передан пустой объект фида");
        return givenFeed;
    } else if(givenFeed.channelUrl.length == 0){
         NSLog(@"В фиде не указан URL");
        return givenFeed;
    } else{
        feed = givenFeed;
        feed.channelTitle = nil; //сбрасываем и далее заново заполняем уже переданное название канала. вдруг оно поменялось, пока мы остутствовали?
        [self parseFeedFromUrl:feed.channelUrl];
        return feed;
    }
}

//Мелкие настройки и сам парсинг, во избежание дублирования кода в двух методах выше.
-(void) doParse{
    //Даем парсеру указание использовать инстанс текущего класса как делегат, содержащий логику обработки xml
    [parser setDelegate:self];
    [parser parse];
}

//Мы можем передать (на перезаполнение) уже заполненный фид, т.е. посмотреть, нет ли чего нового
//в таком случае мы добавляем только те новости, которых еще не было.
//Для такой проверки используем этот метод.
-(BOOL)newItemAlreadyExists:(RSSItem *)newItem{
    for(RSSItem *existentItem in feed.items){
        if([existentItem.title isEqualToString:newItem.title]){
            return YES;
        }
    }
    return NO;
}

@end
