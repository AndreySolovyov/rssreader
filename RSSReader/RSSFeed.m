//
//  RSSFeed.m
//  RSSReader
//
//  Created by Андрей on 18.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSFeed.h"

@implementation RSSFeed

-(id) init{
    self = [super init];
        _items = [[NSMutableArray alloc] init];
        _channelTitle = [[NSMutableString alloc] init];
        _channelImageUrl = [[NSMutableString alloc] init];
        _imagesCache = [[NSCache alloc] init];
    return self;
}

@end
