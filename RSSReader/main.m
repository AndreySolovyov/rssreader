//
//  main.m
//  RSSReader
//
//  Created by Андрей on 11.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSSAppDelegate.h"

int main(int argc, char  *argv[])
{
    @autoreleasepool {
        @try{
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([RSSAppDelegate class]));
        }
        @catch(NSException *ns){
            NSLog(@"%@", [ns description]);
        }
    }
}
