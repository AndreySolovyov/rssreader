//
//  RSSCacher.m
//  RSSReader
//
//  Created by Андрей on 28.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSCacher.h"
#import "RSSItem.h"

@implementation RSSCacher

//Перед тем, как отдать фид на отображение, выкачиваем и кэшируем изображения из фида
//Метод переписывает адрес картинки и использует его как ключ в кэше.
//Раньше ключом было название новости, возможны коллизии

-(void) cacheImagesForFeed:(RSSFeed*)feed{
    static NSString *rssFeedStubPath = @"RSS-Feed-Stub.png";

    for(RSSItem *item in feed.items){
        if([feed.imagesCache objectForKey:item.firstImageUrl] == nil){
            UIImage *image;
            NSString *actualImageUrl = ([item.firstImageUrl length] > 0)?item.firstImageUrl:([feed.channelImageUrl length] > 0)?feed.channelImageUrl:nil;
            //Если есть картинка в enclosure - берем ее. Иначе - логотип канала. Если логотипа нет - заглушку с логотипом RSS.
            if(actualImageUrl == nil){
                image = [UIImage imageNamed:rssFeedStubPath];
                [item setFirstImageUrl:[NSMutableString stringWithString:rssFeedStubPath]];
            } else {
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:actualImageUrl]]];
                if(![item.firstImageUrl isEqualToString:actualImageUrl]){
                    [item setFirstImageUrl:[NSMutableString stringWithString:actualImageUrl]];
                }
            }
            if(image != nil){
                [feed.imagesCache setObject:image forKey:item.firstImageUrl];
            }
        }
    }
}
@end
