//
//  RSSDetailViewController.h
//  RSSReader
//
//  Created by Андрей on 11.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSSItemTableViewController : UIViewController <UISplitViewControllerDelegate, UIWebViewDelegate>

@property NSString *url;
@property (strong, nonatomic) IBOutlet UINavigationItem *navigationBar;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
