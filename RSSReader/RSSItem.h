//
//  RSSItem.h
//  RSSReader
//
//  Created by Андрей on 18.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

//Представляет одну новость из списка
@interface RSSItem : NSObject

@property NSMutableString *link;
@property NSMutableString *title;
@property NSMutableString *pubDate;
@property NSMutableString *description;
@property NSMutableString *firstImageUrl;

@end
