//
//  RSSMasterViewController.h
//  RSSReader
//
//  Created by Андрей on 11.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSFeed.h"

@interface RSSFeedTableViewController : UITableViewController<NSXMLParserDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property RSSFeed *feedToDisplay;

@end
