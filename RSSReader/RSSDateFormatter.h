//
//  RSSDateFormatter.h
//  RSSReader
//
//  Created by Андрей on 16.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSDateFormatter : NSObject

+(NSString*) formatDateFromString:(NSString*)dateAsString;

@end
