//
//  RSSTableCellTableViewCell.m
//  RSSReader
//
//  Created by Андрей on 12.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSTableViewCell.h"

@implementation RSSTableViewCell


@synthesize rssImage = _rssImage;
@synthesize titleLabel = _titleLabel;
@synthesize dateLabel = _dateLabel;
@synthesize descriptionLabel = _descriptionLabel;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
