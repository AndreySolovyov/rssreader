//
//  RSSMasterViewController.m
//  RSSReader
//
//  Created by Андрей on 11.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSFeedTableViewController.h"
#import "RSSItemTableViewController.h"
#import "RSSAvailableFeedsTableViewController.h"

#import "RSSDateFormatter.h"
#import "RSSTableViewCell.h"
#import "RSSFeedParser.h"
#import "RSSItem.h"

const int cellHeight = 86;

@implementation RSSFeedTableViewController{
    NSArray *filteredFeedItems; //Используется при поиске новостей в фиде. Хранит отфильтрованные записи.
    NSArray *feedItems;
    
    RSSFeedParser *rssparser;
    
    //Замечено, что если изображения большие, и метод tableView:cellForRowAtIndexPath:
    //подгружает их для каждой строки таблицы из сети, то скроллинг тормозит.
    //Если хранить изображения в кэше - то нет.
    NSCache *imagesCache;
}

//Инициализатор, вызывающийся при использовании StoryBoard
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self != nil){
        
    }
    return self;
}

//Обработка загрузки объекта из архива Interface Builder-a
- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.searchDisplayController.searchResultsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self prepareView];
}

//Подготавливаем вью к отображению переданного фида
-(void) prepareView{
    feedItems = self.feedToDisplay.items;
    filteredFeedItems = feedItems;
    imagesCache = self.feedToDisplay.imagesCache;

    [self setTitle:self.feedToDisplay.channelTitle];
}

//Подготавливаемся к переходу на новое вью, RSSDetailView, с детальным описанием новости
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"GoToDetails"]){

        NSIndexPath *indexPath;
        RSSItem *item;
        
        if (self.searchDisplayController.active) {
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            item = [filteredFeedItems objectAtIndex:indexPath.row];
        } else {
            indexPath = [self.tableView indexPathForSelectedRow];
            item = [feedItems objectAtIndex:indexPath.row];
        }
        
        NSString *urlToSegue = item.link;
        NSString *titleToSegue = item.title;
        
        RSSItemTableViewController *destination = [segue destinationViewController];
        [destination setUrl:urlToSegue];
        [destination setTitle:titleToSegue];
    }
}

#pragma mark - Table View
//Выводим всегда 86 -  высота строки таблицы
-(CGFloat) tableView:(UITableView*) tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//В зависимости от размера массива с новостями, выдаем количество строк в таблице UITableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        return [filteredFeedItems count];
    } else {
        return [feedItems count];
    }
}

//Обработчик ввода текста в строку поиска
-(void) filterContentForSearchText: (NSString*)searchText scope:(NSString*)scope{
    if([searchText length] >= 3){
        //Фильтруем массив новостей с помощью предиката. Title соответствует проперти объекта item. Мощно и просто.
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", searchText];
        filteredFeedItems = [feedItems filteredArrayUsingPredicate:resultPredicate];
    }
    else
    {
        filteredFeedItems = feedItems;
    }
}

//Перезаполнять ли таблицу с результатами при изменении строки поиска?
- (BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:[
                       [self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]
                      ]
     ];
    return YES;
}

// Логика наполнении строки содержимым из соответствующего элемента item c новостью
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellName = @"RSSTableViewCell";
    
    //Говорим, что используем вместо обычной разметки строки таблицы кастомную -- RSSTableViewCell
    RSSTableViewCell *cell = (RSSTableViewCell*) [tableView dequeueReusableCellWithIdentifier:cellName];
    
    if(cell==nil){
        NSArray *nib =[[NSBundle mainBundle] loadNibNamed:cellName owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    RSSItem *item;
    
    if(tableView == self.searchDisplayController.searchResultsTableView){
        item = [filteredFeedItems objectAtIndex:indexPath.row];
    }
    else{
        item = [feedItems objectAtIndex:indexPath.row];
    }
    
    cell.titleLabel.text = item.title;
    
    NSString *formattedPubDate = [RSSDateFormatter formatDateFromString:item.pubDate];
    
    cell.dateLabel.text = formattedPubDate;
    cell.descriptionLabel.text = item.description;
    
    //Чтобы картинка произвольного размера ужималась в 75х75
    cell.rssImage.contentMode = UIViewContentModeScaleAspectFit;

    //NSNumber *itemHash = [NSNumber numberWithUnsignedInteger:item.hash];
    UIImage *image = [imagesCache objectForKey:item.firstImageUrl];
    
    cell.rssImage.image = image;
    return cell;
}

//Выполняем переход с меткой GoToDetails
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"GoToDetails" sender:nil];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  NO;
}

@end
