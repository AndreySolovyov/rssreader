//
//  RSSAvailableFeedsTableViewController.m
//  RSSReader
//
//  Created by Андрей on 24.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSAvailableFeedsTableViewController.h"
#import "RSSFeedTableViewController.h"
#import "RSSFeedAllowanceChecker.h"

#import "Reachability.h"

#import "RSSFeedParser.h"
#import "RSSCacher.h"
#import "RSSFeed.h"
#import "RSSItem.h"


@implementation RSSAvailableFeedsTableViewController{
    __block RSSFeed *selectedFeed;
    __block BOOL isConnected;
    
    Reachability *internetConnection;
    NSMutableArray *availableFeeds;
    
    RSSFeedParser *rssparser;
    RSSFeedAllowanceChecker *feedAllowanceChecker;
    RSSCacher *cacher;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(availableFeeds == nil){
        [self loadAvailableFeeds];
    }
    
    rssparser = [[RSSFeedParser alloc] init];
    feedAllowanceChecker = [[RSSFeedAllowanceChecker alloc] init];
    cacher = [[RSSCacher alloc] init];
    
    [self setUpGestureRecognizer];
    self.addNewFeedTextField.delegate = self;
    isConnected = YES;
    //[self testInternetConnection];

    [self.tableView reloadData];
}


//В случае касания за пределами UITextField скрываем клавиатуру
//Здесь настраиваем остлеживание касаний
-(void)setUpGestureRecognizer{
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [singleTap setCancelsTouchesInView:NO];
    [self.tableView addGestureRecognizer:singleTap];
}

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    NSString *newFeedUrl = self.addNewFeedTextField.text;
    
    if([newFeedUrl length]==0){
        [self showAlertWithTitle:@"Неверный адрес" andMessage:@"Пустая строка URL"];
        [self hideKeyboard];
        return NO;
    }
    else if(![feedAllowanceChecker isFeedUrlValid:newFeedUrl]){
        [self showAlertWithTitle:@"Неверный адрес" andMessage:@"Неправильный формат URL"];
        [self hideKeyboard];
        return NO;
    } else{
        NSString *alreadyAddedFeedTitle = [feedAllowanceChecker isFeedUrlAlreadyAdded:newFeedUrl toFeedsArray:availableFeeds];
        if(alreadyAddedFeedTitle != nil){
            [self showAlertWithTitle:@"Лента уже добавлена"
                  andMessage:[NSString stringWithFormat:@"По введенному URL уже есть лента\n%@",alreadyAddedFeedTitle]
            ];
            [self hideKeyboard];
            return NO;
        }
    }
    __block RSSFeed *newFeed = [[RSSFeed alloc] init];

    newFeed.channelUrl = [[NSMutableString stringWithString:newFeedUrl] lowercaseString];
    
    [self.addNewFeedTextField setEnabled:NO];
    [self.feedPendingInProgressActivityIndicator startAnimating];
    
    // GCD
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       newFeed = [rssparser populateFeed:newFeed];
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          [self.feedPendingInProgressActivityIndicator stopAnimating];
                                          [self.addNewFeedTextField setEnabled:YES];
                                          
                                          if([feedAllowanceChecker isFeedPotentiallyNotAFeed:newFeed]){
                                              [self showAlertWithTitle:@"Новости не найдены"
                                                                 andMessage:@"Возможно, по введенному адресу нет RSS-ленты"
                                               ];
                                          } else {
                                              [self doneAddingNewFeed:newFeed];
                                          }
                                      });
                   });
    [self hideKeyboard];
    return NO;
}

//Как только лента прогрузилась, очищаем UITextField и убираем клавиатуру
-(void) doneAddingNewFeed:(RSSFeed*) newFeed{
    [availableFeeds insertObject:newFeed atIndex:0];
    [self hideKeyboard];
    [self.addNewFeedTextField setText:@""];
    [self.tableView reloadData];
    [self showAlertWithTitle:@"Лента добавлена" andMessage:newFeed.channelTitle];
}

-(void)showAlertWithTitle:(NSString*)title andMessage:(NSString*)message{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                                    initWithTitle:title message:message
                                    delegate:nil cancelButtonTitle:@"OK"
                                    otherButtonTitles:nil];
    [errorAlert show];
}

-(void) showNoInternetConnectionAlert{
    [self showAlertWithTitle:@"Ошибка подключения" andMessage:@"Нет доступа к интернету"];
}

- (void)hideKeyboard{
    [self.view endEditing:YES];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"FeedContentViewSegue"]){
        RSSFeedTableViewController *destination = [segue destinationViewController];
        destination.feedToDisplay = selectedFeed;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [availableFeeds count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AvailableFeedCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [availableFeeds[indexPath.row] channelTitle];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *) indexPath{
    if(isConnected){
        [self.feedPendingInProgressActivityIndicator startAnimating];
        [self.addNewFeedTextField setEnabled:NO];
        
        //GCD
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           selectedFeed = [rssparser populateFeed:availableFeeds[indexPath.row]];
                           [cacher cacheImagesForFeed:selectedFeed];
                           
                           dispatch_async(dispatch_get_main_queue(),
                                          ^{
                                              [self.addNewFeedTextField setEnabled:YES];
                                              [self.feedPendingInProgressActivityIndicator stopAnimating];
                                              [self performSegueWithIdentifier:@"FeedContentViewSegue" sender:self];
                                          });
                       });
    }
    else {
        [self showNoInternetConnectionAlert];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(!isConnected){
        [self.view endEditing:YES];
        [self showNoInternetConnectionAlert];
        return;
    }
}

//В случае, если изменилось название ленты, изменение отобразилось в UITableView
-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

//Метод, который задает название кнопки удаления
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Удалить";
}

//Метод, обрабатывающий удаление фида, как из TableView, так и из "модели"
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [availableFeeds removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    } 
}

//Заполняем модель тестовыми данными
-(void)loadAvailableFeeds{
    availableFeeds = [[NSMutableArray alloc] init];
    
    RSSFeed *nasa = [[RSSFeed alloc] init];
    nasa.channelTitle = [NSMutableString stringWithString:@"NASA Image of the Day"];
    nasa.channelUrl = @"http://www.nasa.gov/rss/dyn/image_of_the_day.rss";
    [availableFeeds addObject:nasa];
    
    RSSFeed *slash = [[RSSFeed alloc] init];
    slash.channelTitle = [NSMutableString stringWithString:@"Slashdot Developers"];
    slash.channelUrl = @"http://rss.slashdot.org/Slashdot/slashdotDevelopers";
    [availableFeeds addObject:slash];
}

//Метод для проверки соединения с интернетом. Использует сторонний класс.
- (void)testInternetConnection
{
    internetConnection = [Reachability reachabilityWithHostname:@"http://www.ya.ru"];
    
    internetConnection.unreachableBlock = ^(Reachability *reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(isConnected){
                isConnected = NO;
                [self showNoInternetConnectionAlert];
            }
        });
    };

    internetConnection.reachableBlock = ^(Reachability *reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!isConnected){
            isConnected = YES;
            [self showAlertWithTitle:@"Состояние подключения" andMessage:@"Подключение восстановлено"];
            }
        });
    };
    
    [internetConnection startNotifier];
}

@end
