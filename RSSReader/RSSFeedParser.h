//
//  RSSFeedParser.h
//  RSSReader
//
//  Created by Андрей on 15.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSFeed.h"

@interface RSSFeedParser : NSObject<NSXMLParserDelegate>

//Парсить можно из файла (в целях отладки) - не используется
-(RSSFeed*) parseFeedFromContentsOfFile:(NSString*) path withType:(NSString*) type __deprecated;

//И из URL - используется в populateFeed:
-(RSSFeed*) parseFeedFromUrl:(NSString*) url;


//Метод, который заполняет переданный "пустой" объект фида.
//Иcпользуется, так как в настоящий момент при загрузке приложения создаются объекты фидов, у которых заполнен только URL
-(RSSFeed*) populateFeed:(RSSFeed*) givenFeed;


@end
