//
//  RSSCacher.h
//  RSSReader
//
//  Created by Андрей on 28.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSFeed.h"

@interface RSSCacher : NSObject
//Перед тем, как отдать фид на отображение, выкачиваем и кэшируем изображения из фида
-(void) cacheImagesForFeed:(RSSFeed*)feed;
@end
