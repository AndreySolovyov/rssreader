//
//  RSSFeedUrlValidator.m
//  RSSReader
//
//  Created by Андрей on 26.07.14.
//  Copyright (c) 2014 Home. All rights reserved.
//

#import "RSSFeedAllowanceChecker.h"

@implementation RSSFeedAllowanceChecker


static NSRegularExpression *regex;
static NSString *const pattern = @"^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([/\\w \\.-]*)*\\/?$";

+(void) initialize{
    if(self == [RSSFeedAllowanceChecker class]){
        regex = [NSRegularExpression
                 regularExpressionWithPattern:pattern
                 options:0
                 error:nil];
    }
}

-(BOOL)isFeedUrlValid:(NSString*)url{
    NSUInteger num = [regex numberOfMatchesInString:url options:0 range:NSMakeRange(0, [url length])];
    return num == 1;
}

//Отсечением http и www определяем, не добавлена ли уже такая лента
-(NSString*)isFeedUrlAlreadyAdded:(NSString*)url toFeedsArray:(NSArray*)availableFeeds{
    NSString *urlWithoutHttpAndWww = [url lowercaseString];
    
    static NSString *http = @"http://";
    static NSString *https = @"https://";
    static NSString *www = @"www.";
    
    if([urlWithoutHttpAndWww hasPrefix:https]){
        urlWithoutHttpAndWww = [urlWithoutHttpAndWww substringFromIndex:[https length]];
    } else if([urlWithoutHttpAndWww hasPrefix:http]){
        urlWithoutHttpAndWww = [urlWithoutHttpAndWww substringFromIndex:[http length]];
    }
    if([urlWithoutHttpAndWww hasPrefix:www]){
        urlWithoutHttpAndWww = [urlWithoutHttpAndWww substringFromIndex:[www length]];
    }
    
    for(RSSFeed *feed in availableFeeds){
        if([feed.channelUrl hasSuffix:urlWithoutHttpAndWww]){
            return feed.channelTitle;
        }
    }
    return nil;
}

//Если пропарсив фид мы не обнаружии в нем новостей, возможно, по этому адресу вовсе и не фид.
//простая проверка, в случае невыполнения - фид не сохраняем.
-(BOOL)isFeedPotentiallyNotAFeed:(RSSFeed*)feed{
    if(feed.items.count == 0){
        return YES;
    }
    return NO;
}



@end
